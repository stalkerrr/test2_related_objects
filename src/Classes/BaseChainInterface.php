<?php

namespace Dave\Classes;

interface BaseChainInterface
{
    public function addNewEndPoint(array $arr);

    public function calculateLength();
}