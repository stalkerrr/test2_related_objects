<?php

namespace Dave\Classes;

class Point
{
    private $hashCoordinates = ['x' => 0, 'y' => 0];
    private $nextElement;

    public function __get($valueName)
    {
        if($this->hashCoordinates[$valueName]){
            return $this->hashCoordinates[$valueName];
        }
        return false;
    }

    public function __set($valueName, $value)
    {
        if(isset($this->hashCoordinates[$valueName])){
            $this->hashCoordinates[$valueName] = $value;
        }
    }
    
    public function getNextPoint()
    {
        return $this->nextElement;
    }
    
    public function setNextPoint(Point $point)
    {
        $this->nextElement = $point;
    }
    
    public function addAtEnd(Point $topPoint, Point $point)
    {
        while ($topPoint->nextElement != null){
            $topPoint = $topPoint->getNextPoint();
        }

        if($topPoint->y >= $point->y){
            $point->y = ($topPoint->y + 1);
        }
        $topPoint->setNextPoint($point);
    }
}