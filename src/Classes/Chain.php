<?php

namespace Dave\Classes;

class Chain implements BaseChainInterface
{
    private $chain;
    private $trigger;

    private function sortCoordsArray(array $data)
    {
        uasort($data, function ($a, $b){
            if ($a[1] == $b[1]) {
                return 0;
            }
            return ($a[1] < $b[1]) ? -1 : 1;
        });
        return $data;
    }

    public function __construct(array $arr)
    {
        $sortedCoords = $this->sortCoordsArray($arr);
        
        foreach ($sortedCoords as $value){
            $point = new Point();
            $point->x = $value[0];
            $point->y = $value[1];
            
            if(!$this->trigger){
                $this->trigger = true;
                $this->chain = $point;
            }
            else{
                $this->chain->addAtEnd($this->chain,$point);
            }
        }

    }
    public function getChain(){
        return $this->chain;
    }
/*
 * Если координата Y будет меньше, чем у последней точки - она автоматически поменяется на другую.
 */
    public function addNewEndPoint(array $pointCords)
    {
        $point = new Point();
        $point->x = $pointCords[0];
        $point->y = $pointCords[1];

        $this->chain->addAtEnd($this->chain,$point);
    }

    public function calculateLength()
    {
        $current = $this->chain;
        $sum = 0;
        while ($current->getNextPoint()){
            $forY = $current->y - $current->getNextPoint()->y;
            $forX = $current->x - $current->getNextPoint()->x;
            $sum += pow(($forX * $forX + $forY * $forY), 0.5);
            $current = $current->getNextPoint();
        }
        return $sum;
    }
}